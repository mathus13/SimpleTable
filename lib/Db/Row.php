<?php

namespace Ethereal\Db;

use Ethereal\Db\Table;

class Row implements RowInterface
{
    protected $data = array();
    protected $table;

    public function __construct($data, TableInterface $table)
    {
        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
        }
        $this->table = $table;
    }

    /**
     * Gets the value of table.
     *
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * get data attributes
     * @param  string $name data attribute
     * @return        data attribute or NULL
     */
    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }

    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function __call($method, $args)
    {
        if (substr($method, 0, 3) == 'get') {
            $var = substr($method, 3);
            return $this->{$var};
        }
    }

    public function save()
    {
        $this->table->save($this);
    }

    /**
     * Gets the value of data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    public function toArray()
    {
        return $this->getData();
    }

    public function __toString()
    {
        return json_encode($this->getData());
    }
}
